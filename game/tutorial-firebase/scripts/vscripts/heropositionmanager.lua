if HeroPositionManager == nil then
    HeroPositionManager = {}
    
    HeroPositionManager.AUTH_KEY = "DB_Testing"
    if IsDedicatedServer() then
        HeroPositionManager.AUTH_KEY = GetDedicatedServerKeyV2("firebase-example")
    end

    HeroPositionManager.SERVER_LOCATION = "https://dota2-firebase-tutorial.firebaseio.com/"..tostring(HeroPositionManager.AUTH_KEY).."/users/"
end    

function HeroPositionManager:initialize()
    self.initialized = true

    self.trackedHeroes = {}
    self:StartHeroTracking()

    print("HeroPositionManager | Initializing. Key is: "..tostring(HeroPositionManager.AUTH_KEY))

end


function HeroPositionManager:OnHeroInGame(hero)
    local heroname = hero:GetUnitName()

    local steamid = PlayerResource:GetSteamID(hero:GetPlayerID())
    print("HeroPositionManager | Hero: "..heroname.." is in game under control of: "..tostring(steamid))

    self:LoadHeroPosition(steamid, hero)

    self.trackedHeroes[steamid] = hero
end

function HeroPositionManager:StartHeroTracking()
    -- Start the timer
    Timers:CreateTimer(5, function() 
        for steamid, hero in pairs(self.trackedHeroes) do
            self:SaveHeroPosition(steamid, hero)
        end
        -- Time until we run this function again
        return 5
    end)
end

-- Store a pair (steamid, <coordinates>) on our firebase server
function HeroPositionManager:SaveHeroPosition(steamid, hero)
    --print("HeroPositionManager | Saving Hero Position for player: "..tostring(steamid))

    -- Get the hero's position and convert it to a string
    local position = hero:GetAbsOrigin()
    putData = {}
    putData.x_position = position.x
    putData.y_position = position.y
    putData.z_position = position.z

    local encoded = json.encode(putData)

    local request = CreateHTTPRequestScriptVM( "PUT", HeroPositionManager.SERVER_LOCATION..tostring(steamid)..'.json')
    request:SetHTTPRequestRawPostBody("application/json", encoded)

    request:Send( function( result )
        -- Don't do anything with the result (it contains status code information which we might find interesting, though)
    end )
end

-- Load a pair (steamid, <coordinates>) from our firestore
-- Assuming we find something, set our hero to those coordinates
function HeroPositionManager:LoadHeroPosition(steamid, hero)
    --print("HeroPositionManager | Loading Hero Position for player: "..tostring(steamid))

    local request = CreateHTTPRequestScriptVM( "GET", HeroPositionManager.SERVER_LOCATION..tostring(steamid)..'.json')

    request:Send( function( result )

        -- Could we contact the server?
        if not (result.StatusCode == 200) then
            print("HeroPositionManager | Load Failed - Unable to contact server")
            return false
        end

        local obj, pos, err = json.decode(result.Body, 1, nil)

        -- Were we able to retrieve data?
        if not obj then
            print("HeroPositionManager | Load Failed - Could not find any data")
            return false
        end

        -- We got some data, set it!
        hero:SetAbsOrigin(Vector(obj.x_position, obj.y_position, obj.z_position))

    end )

end


if not HeroPositionManager.initialized then
    HeroPositionManager:initialize()
end